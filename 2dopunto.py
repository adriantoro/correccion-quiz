def sum_discs (a_dicts , b_dicts):
    final_dicts ={}
    for key1, val1 in a_dicts.items():
        for key2, val2 in b_dicts.items():
            if key1==key2:
                final_dicts.setdefault(key1, val1+val2)
                break
    return final_dicts

a_dicts ={"num1": 2, "num2":5}
b_dicts ={"num1": 4, "num2": 7}

print(sum_discs (a_dicts , b_dicts))
